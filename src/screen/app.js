import React, { Component } from "react";
import { connect } from "react-redux";
import {
  SocialMedia,
  Logo,
  VideoHeader,
  YoutubeVideo,
  MyButtons,
  Subtitels,
  SearchBar,
  AboutUs
} from "../components";
import { Content, Container, Footer} from "native-base";
class App extends Component {
  render() {
    const {captions}=this.props.youtube
    return (
      <Container>
        <Logo />
        <Content>
          <SearchBar />
          {captions.length>0?
          [
          <VideoHeader />,
          <YoutubeVideo />,
          <MyButtons />,
          <Subtitels />
          ]
          :<AboutUs/>
}
        </Content>
        <Footer>
          <SocialMedia />
        </Footer>
      </Container>
    );
  }
}
const mapStateToProps = state => ({
  youtube: state.myState.youtube
});
const mapActionToProps = {};

export default connect(
  mapStateToProps,
  mapActionToProps
)(App);
