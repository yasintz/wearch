import { createDrawerNavigator,createSwitchNavigator} from "react-navigation";
import Animation from "./animation";
import App from "./app";

export default createSwitchNavigator(
  {
    Animation: {
      screen: Animation,
    },
    App
  }
);
