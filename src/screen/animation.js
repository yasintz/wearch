import React from "react";
import { View, ActivityIndicator, Dimensions } from "react-native";
import {
  getCaption,
  searchCaptionGet,
  addYoutubeConfig
} from "../redux/actions";
import { connect } from "react-redux";
const { width } = Dimensions.get("window");
import { captionGet } from "../lib";
import I18n from "react-native-i18n";
import lang, { languageList } from "../language";
class AnimationScreen extends React.Component {
  constructor(props) {
    super(props);
    this.getCaption = this.getCaption.bind(this);
    this.state = {
      status: true
    };
  }
  getCaption = () => {
    const systemLanguage = I18n.currentLocale().substring(0, 2);
    const isThereSystemLanguage = languageList.find(
      item => item === systemLanguage
    );
    isThereSystemLanguage && lang.setLanguage(systemLanguage);
    const { getCaption, searchCaptionGet, navigation } = this.props;
    const about = d => this.props.addYoutubeConfig({ aboutUs: d });
    captionGet(getCaption, searchCaptionGet, navigation.navigate, about);
    this.setState({ status: false });
  };
  render() {
    this.state.status && this.getCaption();
    return (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <ActivityIndicator size={width / 1.3} color="#398DC9" />
      </View>
    );
  }
}

const mapStateToProps = state => ({
  captions: state.myState.captions,
  searchCaption: state.myState.searchCaption
});
const mapActionsToProps = {
  getCaption,
  searchCaptionGet,
  addYoutubeConfig
};
export default connect(
  mapStateToProps,
  mapActionsToProps
)(AnimationScreen);
