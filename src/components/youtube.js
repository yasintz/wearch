import React, { Component } from "react";
import YouTube from "react-native-youtube";
import { Dimensions, Image } from "react-native";
import { addYoutubeConfig } from "../redux/actions";
import { connect } from "react-redux";
const { width, height } = Dimensions.get("screen");
class Youtube extends Component {
  youtubeConfig = () => {
    const { youtube, addYoutubeConfig } = this.props;
    const { captions, videoIndex } = youtube;
    let { id } = captions[videoIndex];
    return {
      videoId: id,
      play: true,
      apiKey: "AIzaSyDeKOi8dXYZhZKxzLVfglkeL8QQUGEwJ5o",
      ref: component => {
        if (component) {
          if (youtube.id !== id) {
            addYoutubeConfig({ youtubeVideo: component, id });
          }
        }
      },
      onReady: e => addYoutubeConfig({ isReady: true }),
      style: { alignSelf: "stretch", height: width / 1.775, width: width }
    };
  };
  render() {
    return <YouTube {...this.youtubeConfig()} />;
  }
}

const mapStateToProps = state => ({
  searchCaption: state.myState.searchCaption,
  normalCaption: state.myState.captions,
  youtube: state.myState.youtube
});
const mapActionToProps = {
  addYoutubeConfig
};
export default connect(
  mapStateToProps,
  mapActionToProps
)(Youtube);
