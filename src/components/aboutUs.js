import lang from "../language";
import React, { Component } from "react";
import { Text, View, Dimensions } from "react-native";
import { connect } from "react-redux";
const { width, height } = Dimensions.get("window");
import { addYoutubeConfig } from "../redux/actions";
const AboutUs = ({ youtube }) => {
  return (
    <View
      contentContainerStyle={{
        width: width,
        height: height / 1.39,
        alignItems: "center",
        marginTop: width / 20
      }}
    >
      <Text
        style={{
          color: "#A8A4A4",
          fontFamily: "Aclonica",
          fontSize: 11.15,
          width: (width / 5) * 4
        }}
      >
        {youtube.aboutUs[lang.getLanguage()]}
      </Text>
    </View>
  );
};
const mapStateToProps = state => ({
  youtube: state.myState.youtube
});
const mapActionToProps = { addYoutubeConfig };
export default connect(
  mapStateToProps,
  mapActionToProps
)(AboutUs);
