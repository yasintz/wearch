import React, { Component } from "react";
import { Text, View, Dimensions, TouchableOpacity, Alert } from "react-native";
import { Icon } from "react-native-elements";
import { connect } from "react-redux";
import { addYoutubeConfig } from "../redux/actions";
import lang from "../language";
const { width, height } = Dimensions.get("window");
const size = (Math.sqrt(height * width) / 100) * 8.4;
class buttons extends Component {
  myAlert = (title, message) => {
    Alert.alert(title, message, [{ text: "OK", onPress: () => {} }], {
      cancelable: false
    });
  };
  setVideoIndex = t => {
    const { captions, videoIndex } = this.props.youtube;

    let cpLength = captions.length;
    t
      ? videoIndex + 1 !== cpLength
        ? this.props.addYoutubeConfig({
            videoIndex: videoIndex + 1,
            showingCaptionIndex: undefined
          })
        : this.myAlert("OOOPS!!!", lang.lastVideo)
      : videoIndex === 0
        ? this.myAlert("OOOPS!!!", lang.firstVideo)
        : this.props.addYoutubeConfig({
            videoIndex: videoIndex - 1,
            showingCaptionIndex: undefined
          });
  };
  render() {
    const { youtubeVideo, captions, videoIndex } = this.props.youtube;
    let startingTime =captions[videoIndex].caption[0][0];
    return (
      <View
        flexDirection="row"
        width={width - 50}
        height={height / 9}
        justifyContent="space-between"
        alignItems="center"
        marginLeft={25}
      >
        <TouchableOpacity onPress={() => this.setVideoIndex(false)}>
          <Icon
            name="skip-previous-circle-outline"
            type="material-community"
            color="#398DC9"
            size={size}
          />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            captions.length > 0 && youtubeVideo.seekTo(startingTime);
          }}
        >
          <Icon
            name="restart"
            type="material-community"
            color="#398DC9"
            size={size}
          />
        </TouchableOpacity>
        <TouchableOpacity onPress={() => this.setVideoIndex(true)}>
          <Icon
            name="skip-next-circle-outline"
            type="material-community"
            color="#398DC9"
            size={size}
          />
        </TouchableOpacity>
      </View>
    );
  }
}
const mapStateToProps = state => ({
  youtube: state.myState.youtube
});
const mapActionToProps = {
  addYoutubeConfig
};
export default connect(
  mapStateToProps,
  mapActionToProps
)(buttons);
