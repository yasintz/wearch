import React, { Component } from "react";
import { Search, arraymixing } from "../lib";
import {
  View,
  Text,
  Dimensions,
  StyleSheet,
  TouchableOpacity
} from "react-native";
import { connect } from "react-redux";
import lang from "../language";
const { width, height } = Dimensions.get("window");

class videoHeader extends Component {
  state = {
    inputShowing: false
  };
  render() {
    const { youtube } = this.props;

    let { captions, searchWord, videoIndex } = youtube;
    return (
      <View style={styles.container}>
        <View
          style={{
            flex: 45,
            justifyContent: "center",
            marginLeft: 18
          }}
        >
          <Text style={styles.hederText}>
            {lang.headerText(searchWord, captions.length)}
          </Text>
        </View>
        <View
          style={{
            flex: 5,
            justifyContent: "center",
            marginTop: -5,
            alignItems: "center"
          }}
        >
          <Text style={styles.hederText}>
            {videoIndex + 1}/{captions.length}
          </Text>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    width,
    height: height / 13,
    flexDirection: "row",
    paddingRight: 10,
    alignItems: "center",
    justifyContent: "center"
  },
  hederText: {
    color: "#A8A4A4",
    fontFamily: "Aclonica",
    fontSize: 11.15
  }
});
const mapStateToProps = state => ({
  searchCaption: state.myState.searchCaption,
  normalCaption: state.myState.captions,
  youtube: state.myState.youtube
});
const mapActionToProps = {};

export default connect(
  mapStateToProps,
  mapActionToProps
)(videoHeader);
