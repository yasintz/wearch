import React, { Component } from "react";
import { Search, arraymixing } from "../lib";
import { addYoutubeConfig } from "../redux/actions";
import { View, Dimensions, TouchableOpacity, Alert } from "react-native";
import { connect } from "react-redux";
import { Icon } from "react-native-elements";
import lang from "../language";
import { Input } from "native-base";
const { width, height } = Dimensions.get("window");

class search extends Component {
  state = {
    containerView: undefined
  };
  myAlert = (title, message) => {
    Alert.alert(title, message, [{ text: "OK", onPress: () => {} }], {
      cancelable: false
    });
  };
  searching = () => {
    const {
      searchCaption,
      normalCaption,
      addYoutubeConfig,
      youtube
    } = this.props;
    const resultCaption = arraymixing(
      Search(youtube.text.toLowerCase().trim(), normalCaption, searchCaption)
    );
    if (resultCaption.length > 0) {
      youtube.youtubeVideo&&youtube.youtubeVideo.seekTo(0);
      addYoutubeConfig({
        captions: resultCaption,
        videoIndex: 0,
        searchWord: youtube.text.toLowerCase().trim(),
        showingCaptionIndex: false
      });
    } else {
      this.myAlert("", lang.notFound);
    }
  };
  render() {
    const { addYoutubeConfig, youtube } = this.props;
    return (
      <View
        style={{ ...styles.container, height: height / 18, width: width - 10 }}
      >
        <View
          style={{
            width: (width / 25) * 21 - 28,
            height: height / 18,
            justifyContent: "center",
            marginLeft: 18
          }}
        >
          <Input
            style={styles.TextInput}
            onChangeText={t => addYoutubeConfig({ text: t })}
            value={youtube.text}
            placeholder={lang.placeholder}
            underlineColorAndroid="transparent"
            placeholderTextColor="#9A9696"
          />
        </View>
        <TouchableOpacity
          onPress={() => {
            youtube.text && this.searching();
            addYoutubeConfig({ text: "" });
          }}
          style={{
            height: height / 18,
            width: (width / 25) * 4 - 3,
            justifyContent: "center",
            alignItems: "center",
            backgroundColor: "#398DC9",
            borderRadius: 10
          }}
        >
          <Icon
            name="search"
            type="font-awesome"
            color="white"
            size={22}
            containerStyle={{ marginTop: -2 }}
          />
        </TouchableOpacity>
      </View>
    );
  }
}
const styles = {
  container: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    borderColor: "#398DC9",
    borderWidth: 1.5,
    borderRadius: 10,
    backgroundColor: "#E3E3E3",
    margin: 5
  },
  TextInput: {
    flex: 1,
    color: "#9A9696",
    padding: 3,
    fontFamily: "Aclonica",
    fontSize: 11.15
  }
};
const mapStateToProps = state => ({
  searchCaption: state.myState.searchCaption,
  normalCaption: state.myState.captions,
  youtube: state.myState.youtube
});
const mapActionToProps = { addYoutubeConfig };

export default connect(
  mapStateToProps,
  mapActionToProps
)(search);
