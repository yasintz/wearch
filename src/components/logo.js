import React, { Component } from "react";
import { View, Text, Dimensions, StyleSheet, Image } from "react-native";
import { logo } from "../../static";
import { connect } from 'react-redux'
const { width, height } = Dimensions.get("window");
class Logo extends Component {
  render() {
    return <Image style={styles.logo} source={logo} />;
  }
}
const styles = StyleSheet.create({
  logo: {
    width: width,
    height: (height / 100) * 6,
    marginBottom:height/40
  }
});
const mapStateToProps = state => ({});

const mapActionToProps = {};

export default connect(
  mapStateToProps,
  mapActionToProps
)(Logo);
