import SocialMedia from "./social_media";
import Logo from "./logo";
import VideoHeader from "./videoHeader";
import YoutubeVideo from "./youtube";
import MyButtons from "./buttons";
import Subtitels from './subtitels'
import SearchBar from "./searchBar"
import AboutUs from './aboutUs'
export {AboutUs,SearchBar,SocialMedia, Logo, VideoHeader, YoutubeVideo, MyButtons,Subtitels };
