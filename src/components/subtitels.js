import React, { Component } from "react";
import { addYoutubeConfig } from "../redux/actions";
import { connect } from "react-redux";
import { Text, Dimensions, View } from "react-native";
const { width, height } = Dimensions.get("window");
class subtitels extends Component {
  componentDidMount() {
    this._interval = setInterval(() => {
      const { normalCaption, youtube, addYoutubeConfig } = this.props;
      const {
        youtubeVideo,
        currentTime,
        isReady,
        captions,
        videoIndex
      } = youtube;
    const startingTime = captions[videoIndex].caption[0][0];
      if (youtubeVideo && isReady) {
        youtubeVideo.currentTime().then(currentT => {
          if (currentT === 0) {
            youtubeVideo.seekTo(startingTime);
          } else {
            if (currentT !== currentTime) {
              const selectCaption = captions[videoIndex];
              const video = normalCaption.find(
                ({ id }) => selectCaption.id === id
              );
              const getIndex = video.caption.findIndex(
                cp => cp[0][0] === currentT
              );
              addYoutubeConfig({ currentTime: currentT });
              if (getIndex !== -1) {
                addYoutubeConfig({ showingCaptionIndex: getIndex });
              }
            }
          }
        });
      }
    }, 300);
  }
  componentWillMount() {
    clearInterval(this._interval);
  }
  render() {
    const { normalCaption, youtube } = this.props;
    const { captions, videoIndex, showingCaptionIndex } = youtube;
    const selectCaption = captions[videoIndex];
    const video = normalCaption.find(({ id }) => selectCaption.id === id);
    return (
      <View
        width={width - 50}
        marginLeft={25}
        height={height / 7}
        justifyContent="center"
      >
        <Text
          style={{
            fontFamily: "Abysncsil",
            width: width - 50,
            fontSize: 15
          }}
        >
          {showingCaptionIndex
            ? video.caption[showingCaptionIndex][1]
            : selectCaption.caption[1]}
        </Text>
      </View>
    );
  }
}
const mapStateToProps = state => ({
  normalCaption: state.myState.captions,
  youtube: state.myState.youtube
});
const mapActionToProps = {
  addYoutubeConfig
};
export default connect(
  mapStateToProps,
  mapActionToProps
)(subtitels);
