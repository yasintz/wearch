import React from "react";
import {
  View,
  Dimensions,
  Image,
  TouchableOpacity,
  Linking
} from "react-native";
import Mailer from "react-native-mail";
import { socialMedia, gmail } from "../../static";
const { width, height } = Dimensions.get("window");
let iconSize = Math.sqrt(((width * height) / 100) * 0.449589491);
let mrg = (width - iconSize * 6) / 7;
const mailSend = () => {
  Mailer.mail(
    {
      subject: "",
      recipients: ["wearch.app@gmail.com"],
      ccRecipients: [],
      bccRecipients: [],
      body: "",
      isHTML: true,
      attachment: {
        path: "", // The absolute path of the file from which to read data.
        type: "", // Mime Type: jpg, png, doc, ppt, html, pdf, csv
        name: "" // Optional: Custom filename for attachment
      }
    },
    (error, event) => {
      Alert.alert(
        error,
        event,
        [
          {
            text: "Ok",
            onPress: () => {}
          },
          {
            text: "Cancel",
            onPress: () => {}
          }
        ],
        { cancelable: true }
      );
    }
  );
};

export default () => (
  <View
    style={{
      alignItems: "center",
      flexDirection: "row",
      flex: 1,
      backgroundColor: "#F2EEEE"
    }}
  >
    {socialMedia.map((v, i) => {
      return (
        <TouchableOpacity onPress={() => Linking.openURL(v.url)} key={i}>
          <Image
            style={{
              width: iconSize,
              height: iconSize,
              marginLeft: mrg
            }}
            source={v.source}
          />
        </TouchableOpacity>
      );
    })}
    <TouchableOpacity onPress={() => mailSend()}>
      <Image
        style={{
          width: iconSize,
          height: iconSize,
          marginLeft: mrg
        }}
        source={gmail}
      />
    </TouchableOpacity>
  </View>
);
