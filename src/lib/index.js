import captionGet from './captionGet'
import Search from './search'
import arraymixing from './arraymixing'
import {save,readAboutUs} from './arrays'
export {
  captionGet,
  readAboutUs,
  Search,
  arraymixing,
  save
}