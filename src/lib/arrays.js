import firebase from "firebase";
import axios from "axios";
export let replace = [
  "(",
  ")",
  "[",
  "]",
  ",",
  ".",
  "/",
  "#",
  "!",
  "$",
  "%",
  "~",
  ";",
  ":",
  "{",
  "}",
  "&",
  "?",
  "^",
  "_",
  "-"
];
var config = {
  apiKey: "AIzaSyAG6j2L6c_6eGrnAm0liGDZ05VM4yY3sQY",
  authDomain: "wearch-291916.firebaseapp.com",
  databaseURL: "https://wearch-291916.firebaseio.com",
  projectId: "wearch-291916",
  storageBucket: "",
  messagingSenderId: "551860587369"
};
firebase.initializeApp(config);

export const read =(handle,about) => {
  firebase
    .database()
    .ref()
    .once("value", d => {
      const data=d.toJSON()
      let ids = Object.keys(data.videos);
      // ids = ids.slice(0, 2);
      ids.map(id => {
        handle(id, ids.length);
      });
      about(data.aboutUs)
    });
};
export const save = id => {
  axios
    .get(`https://www.youtube.com/api/timedtext?lang=en&fmt=vtt&name=&v=${id}`)
    .then(({ data }) => {
      if (data) {
        let o = {};
        o[id] = {
          status: true
        };
        firebase
          .database()
          .ref("/videos")
          .update(o);
      }
    })
    .catch(error => console.log(error));
};
