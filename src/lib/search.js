export default (word,showData , searchData) => {
  let myData = [];
  let wordArray = word.split(" ");

  let single = () => {
    searchData.map((video, iTwo) => {
      video.caption.map((c, iThree) => {
        let indexOfFirst = c[1].indexOf(word);
        if (c[1].indexOf(word) >= 0) {
          c[1].split(" ").map((cw, i) => {
            if (word === cw) {
              myData.push({
                caption: showData[iTwo].caption[iThree],
                id: video.id,
                subs: [indexOfFirst, indexOfFirst + word.length],
                keys:{video:iTwo,caption:iThree}
              });
            }
          });
        }
      });
    });
  };
  let multi = () => {
    searchData.map((video, iTwo) => {
      video.caption.map((c, iThree) => {
        let indexOfFirst = c[1].indexOf(word);
        if (indexOfFirst >= 0) {
          if (
            c[1].substring(
              indexOfFirst + word.length,
              indexOfFirst + word.length + 1
            ) === " "
          ) {
            myData.push({
              caption: showData[iTwo].caption[iThree],
              id: video.id,
              subs: [indexOfFirst, indexOfFirst + word.length]
            });
          }
        }
      });
    });
  };
  if (wordArray.length > 1) {
    multi();
  } else {
    single();
  }
  return myData;
};
