import axios from "axios";
import { IDs,read } from "./arrays";
import Replace from "./replace";
export default (NcaptionSave, ScaptionSave, navigate,about) => {
  let counter = 0;
  const mapFunctation=(id,length) => {
    axios
      .get(
        `https://www.youtube.com/api/timedtext?lang=en&fmt=vtt&name=&v=${id}`
      )
      .then(({ data }) => {
        NcaptionSave({ id: id, caption: getCaptions(data).writeCaption });
        ScaptionSave({ id: id, caption: getCaptions(data).searchCaption });
        counter++;
        length === counter &&navigate("App");
      })
      .catch(error => console.log(error));
  };
  read(mapFunctation,about)
};

const getCaptions = caption => {
  const captions = [];
  let mydata = caption.split("\n");
  captions.push(mydata[3]);
  mydata.map((item, index) => {
    captions.push(item);
  });
  return combineExtract(captions);
};

const combineExtract = cp => {
  const caption = cp;
  let blank = [];
  let mycaption = [];
  let searchCaption = [];
  let intermediate = 0;
  caption.map((item, index) => {
    if (item === "") {
      blank.push(index + 1);
    }
  });
  blank.map((item, index) => {
    let temporary = "";

    let get = blank[index + 1] - item - 1;
    for (let i = intermediate + 1; i < get + intermediate; i++) {
      temporary += caption[i] + " ";
    }

    let smash =
      caption[intermediate] !== undefined
        ? caption[intermediate].split(" --> ")
        : [];
    smash = smash.map(item => item.split(":"));
    let combine = [];
    smash.map((item, i) => {
      let time = +item[2] + +item[1] * 60 + +item[0] * 60 * 60;
      time = i === 0 ? parseInt(time) : parseInt(time) + 1;
      combine.push(time);
    });
    mycaption.push([combine, temporary.trim()]);
    searchCaption.push([combine, Replace(temporary.trim())]);

    intermediate = blank[index + 1];
  });
  let captions = [];
  let sCp = [];
  for (let i = 1; i < mycaption.length - 2; i++) captions.push(mycaption[i]);
  for (let i = 1; i < searchCaption.length - 2; i++) sCp.push(searchCaption[i]);
  return { writeCaption: captions, searchCaption: sCp };
};
