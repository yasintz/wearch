import { combineReducers } from "redux";
import captions from "./caption.reducer";
import searchCaption from './searchCaption.reducer'
import youtube from './youtube.reducer'
export default combineReducers({
  captions,searchCaption,youtube
});
