import { YOUTUBE_CONFIG } from "../actions";
const initialState = {
  captions: [],
  isPlaying: false,
  videoIndex: 0,
  calling: false,
};
export default (state = initialState, { type, pyload })=>{
  switch (type) {
    case YOUTUBE_CONFIG:
      return { ...state, ...pyload};
    default:
      return state;
  }
}
