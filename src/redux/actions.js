export const CAPTION_GET= "CAPTION_GET";
export const SEARCH_CAPTION_GET= "SEARCH_CAPTION_GET";
export const YOUTUBE_CONFIG="YOUTUBE_CONFIG";



export function getCaption(value) {
  return {
    type:CAPTION_GET ,
    pyload: value
  };
}
export function searchCaptionGet(value){
  return{
    type:SEARCH_CAPTION_GET,
    pyload:value
  }
}
export function addYoutubeConfig(value){
return{
    type:YOUTUBE_CONFIG,
    pyload:value
  }
}
