import LocalizedStrings from "react-native-localization";
import languageList from './list'

export default new LocalizedStrings({
  tr: {
    headerText:(word,length) =>`"${word}" için ${length} sonuç bulundu`,
    placeholder:"aramak istediğiniz kelimeyi girin...",
    notFound:"Video Bulunamadı!",
    lastVideo:"Son Videodasın",
    firstVideo:"İlk Videodasın",
    aboutUs:"hakkimizda",
  },
  en: {
    headerText:(word,length) =>`There were ${length} videos for ${word}`,
    placeholder:"write the search word...",
    notFound:"Not Found!",
    lastVideo:"Last Video",
    firstVideo:"First Video",
    aboutUs:"about us",
    }
});

export {
languageList
}