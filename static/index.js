import logo from "./logom.png";
import patreon from "./patreon.png";
import github from "./github.png";
import gmail from "./gmail.png";
import linkedin from "./linkedin.png";
import twitter from "./twitter.png";
import instagram from "./instagram.png";
import vnf from './video-not-found.png'
const socialMedia = [
  { source: linkedin, url: "https://www.linkedin.com/in/yasintazeoglu/" },
  { source:twitter, url: "https://twitter.com/yasin_tazeoglu" },
  { source:instagram, url: "https://www.instagram.com/yasinntazeoglu/" },
  { source:github, url: "https://github.com/yasintazeoglu" },
  { source:patreon, url: "https://www.patreon.com/wearch" },
];
export { logo, socialMedia,gmail,vnf };
