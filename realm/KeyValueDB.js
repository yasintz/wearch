import Realm from "realm";
export const KEY_VALUE_SCHEMA = "KEY_VALUE_SCHEMA";

export const KeyValueSchema = {
  name: KEY_VALUE_SCHEMA,
  primaryKey: "key",
  properties: { key: "string", value: "string" }
};
const databaseOption = {
  path: "test2db.realm",
  schema: [KeyValueSchema]
};
export const insertOrUpdate = ({ key, value }) =>
  new Promise((resolve, reject) => {
    Realm.open(databaseOption)
      .then(realm => {
        realm.write(() => {
          queryData(key)
            .then(v => {
              update({ key, value });
            })
            .catch(e => {
              const typ = typeof value;
              const myValue = typ === "string" ? value : JSON.stringify(value);
              realm.create(KEY_VALUE_SCHEMA, { key, value: myValue });
              resolve({ key, value: myValue });
            });
        });
      })
      .catch(e => reject(e));
  });

export const queryAllData = () =>
  new Promise((resolve, reject) => {
    Realm.open(databaseOption)
      .then(realm => {
        const allData = realm.objects(KEY_VALUE_SCHEMA);
        const results = allData.map(v => ({
          key: v.key,
          value: JSON.parse(v.value)
        }));
        resolve(results);
      })
      .catch(e => reject(e));
  });

export const deleteData = key =>
  new Promise((resolve, reject) => {
    Realm.open(databaseOption)
      .then(realm => {
        realm.write(() => {
          const deletingData = realm.objectForPrimaryKey(KEY_VALUE_SCHEMA, key);
          realm.delete(deletingData);
          resolve();
        });
      })
      .catch(e => reject(e));
  });
export const deleteAllData = () =>
  new Promise((resolve, reject) => {
    Realm.open(databaseOption)
      .then(realm => {
        realm.write(() => {
          const deletingData = realm.objects(KEY_VALUE_SCHEMA);
          realm.delete(deletingData);
          resolve();
        });
      })
      .catch(e => reject(e));
  });
export const queryData = key =>
  new Promise((resolve, reject) => {
    Realm.open(databaseOption)
      .then(realm => {
        realm.write(() => {
          const data = realm.objectForPrimaryKey(KEY_VALUE_SCHEMA, key);
          resolve({ key: data.key, value: data.value });
        });
      })
      .catch(e => reject(e));
  });
const update = ({ key, value }) =>
  new Promise((resolve, reject) => {
    Realm.open(databaseOption)
      .then(realm => {
        realm.write(() => {
          const typ = typeof value;
          const myValue = typ === "string" ? value : JSON.stringify(value);
          const data = realm.objectForPrimaryKey(KEY_VALUE_SCHEMA, key);
          data.value = myValue;
          resolve();
        });
      })
      .catch(e => reject(e));
  });
