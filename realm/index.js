import React, { Component } from "react";
import { StyleSheet, Text, View, ScrollView } from "react-native";
import {
  insertOrUpdate,
  deleteAllData,
  deleteData,
  queryAllData,
  queryData
} from "./KeyValueDB";
export default class App extends Component {
  state = { text: "no data", data: [] };
  get = async () => {
    const allData = await queryAllData();
    const deleteSingle = await deleteData("mykeyim");
    const singleData = await queryData("mykeyim");
    const deleteAll = await deleteAllData();
    const setting = await insertOrUpdate({
      key: "mykeyim2",
      value: { name: "osman", surname: "maresal" }
    });
  };
  render() {
    return (
      <View style={styles.container}>
        <Text onPress={this.get}>{this.state.text}</Text>
        <ScrollView contentContainerStyle={styles.maping}>
          {/* {this.state.data.map(v => (
            <Text>{JSON.stringify(v.value)}</Text>
          ))} */}
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  maping: {
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  }
});
